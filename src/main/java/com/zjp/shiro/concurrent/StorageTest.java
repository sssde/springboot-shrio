package com.zjp.shiro.concurrent;

import java.util.ArrayList;
import java.util.List;

import com.zjp.shiro.redis.JedisUtil;

import redis.clients.jedis.JedisCluster;

public class StorageTest {

	public static void main(String[] args) {
		JedisUtil jedisUtil = new JedisUtil();
		jedisUtil.setNodes("bluntly.redis1:6300;bluntly.redis2:6300;bluntly.redis3:6300");
		JedisCluster redis = jedisUtil.getJedisCluster();
		redis.set("xxx-storage", "10");
		List<Thread> threadList = new ArrayList<Thread>(100);
		for(int i = 0;i<100;i++){
			Thread buyThread = new Thread(new BuyThread(redis));
			threadList.add(buyThread);
		}
		for(Thread thread : threadList){
			thread.start();
		}
	}

	
}

class BuyThread implements Runnable{

	private JedisCluster redis;
	public BuyThread(JedisCluster redis){
		this.redis = redis;
	}
	@Override
	public void run() {
		long storage = redis.decr("xxx-storage");
		if(storage < 0){
			System.out.println(Thread.currentThread().getName()+"没抢到");
		}else{
			System.err.println(Thread.currentThread().getName()+"抢到了");
		}
		
	}
}