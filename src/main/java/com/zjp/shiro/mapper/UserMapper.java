package com.zjp.shiro.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.zjp.shiro.pojo.User;

public interface UserMapper {
	
	@Insert("insert into user(name,age) values (#{name},#{age})")
	int insert(@Param("name") String name,@Param("age") int age);
	
	@Select("select * from user where name = #{name}")
	User queryUserByName(@Param("name") String name);
}
