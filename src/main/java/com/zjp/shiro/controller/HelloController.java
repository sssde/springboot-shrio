package com.zjp.shiro.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.mgt.FilterChainManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import redis.clients.jedis.JedisCluster;

@Controller
public class HelloController {
	
	@RequestMapping("/exception")
	public String exception() throws Exception {
	    throw new Exception("发生错误");
	}
	
	@RequestMapping({"/","/index"})
    public String index(){
        return "index";
    }
	@RequestMapping("/login")
    public String login(HttpServletRequest request ){
	    return "/login";
    }
	
	@RequestMapping("/userInfo")
    public String userInfo(HttpServletRequest request ){
	    return "/userInfo";
    }
	@RequestMapping("/add")
    public String add(HttpServletRequest request ){
	    return "/add";
    }

    @ApiOperation(value="登录", notes="用户登录")
    @ApiImplicitParams({
    	  @ApiImplicitParam(name = "username", value = "用户名", required = true, dataType = "String"),
    	  @ApiImplicitParam(name = "password", value = "用户密码", required = true, dataType = "String")
    })
    @RequestMapping(value="/doLogin", method=RequestMethod.POST)
    public String doLogin(String username,String password){
    	Map<String,Object> resultMap = new HashMap<String, Object>();
    	try {
    		UsernamePasswordToken token = new UsernamePasswordToken(username, password); 
//    		token.setRememberMe(true);
    		SecurityUtils.getSubject().login(token);
    		System.out.println(SecurityUtils.getSubject().isAuthenticated());
    		resultMap.put("status", 200);
    		resultMap.put("message", "登录成功");
    	} catch (Exception e) {
    		e.printStackTrace();
    		resultMap.put("status", 500);
    		resultMap.put("message", e.getMessage());
    	}
    	return "index";
    }
}
